public class Application{
	public static void main(String[] args){
		// q4
		Student april = new Student();
		april.name = "April";
		april.age = 18;
		april.cute = true;
		
		Student tin = new Student();
		tin.name = "Tin";
		tin.age = 17;
		tin.cute = true;
		
		//q6
		System.out.println(april.name);
		System.out.println(tin.cute);
		
		//q8
		april.sayName();
		tin.isCute();
		
		//q13
		Student[] section3 = new Student[3];
		
		//q14
		section3[0] = april;
		section3[1] = tin;
		
		//q15
		System.out.println(section3[0].name);
		
		//q20
		section3[2] = new Student();
		
		//q21
		section3[2].name = "Tinothy";
		section3[2].age = 17;
		section3[2].cute = false;
		
		System.out.println(section3[2].name + ", " + section3[2].age + ", " + section3[2].cute);
	}
}